//
//  ronan_desafio_iosTests.swift
//  ronan-desafio-iosTests
//
//  Created by Ronan on 11/2/17.
//  Copyright © 2017 RONAN. All rights reserved.
//

import XCTest
@testable import ronan_desafio_ios

class ronan_desafio_iosTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testIsGitApiUrlCorrectly() {
        let url = GitServer()
        
        XCTAssertEqual(url.base, "https://api.github.com")
    }
    
    func testTestingitensPerPage() {
        let itens = ContentManager()
        
        XCTAssertEqual(itens.itensPerPage, 20)
        
    }
    
}
