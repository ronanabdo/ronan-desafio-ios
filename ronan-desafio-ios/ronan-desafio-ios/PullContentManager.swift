//
//  PullContentManager.swift
//  ronan-desafio-ios
//
//  Created by Ronan on 11/3/17.
//  Copyright © 2017 RONAN. All rights reserved.
//
// Referencias:
// Udemy - Devslopes
// Projeto - Bruno Eiti


import UIKit

class PullContentManager: NSObject {
    
    private  var loadedPulls = [PullData]()
    private  var loading = false
    private  let server = GitServer()
    
    public  func getContent() -> [PullData]{
        return loadedPulls;
    }
    //everytime the content manager need more data to send to UI,
    //this methods download more data and make it ready
    public  func askContent(login:String,repository:String,callBack:@escaping (Bool)->Void) -> Void{
        if(!loading){
            
            server.getJavaPulls(login: login, repository: repository, callback: { (data) in
                if let data = data{
                    self.loadedPulls = data
                    callBack(true)
                }else{
                    callBack(false)
                }
            })
            
        }
    }
    
    
}
