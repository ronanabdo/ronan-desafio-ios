//
//  PullData.swift
//  ronan-desafio-ios
//
//  Created by Ronan on 11/3/17.
//  Copyright © 2017 RONAN. All rights reserved.
//
// Referencias:
// Udemy - Devslopes
// Projeto - Bruno Eiti


import UIKit
import  ObjectMapper
/*
 Maps JSON fields of a Pull Request from GIT API to Swift Object
 using ObjectMapper
 */
class  PullData: Mappable {
    var id: Int?
    var title: String?
    var body: String?
    var user : UserData?
    var url : String?
    var state: String?
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        id <- map["id"]
        title <- map["title"]
        body <- map["body"]
        user <- map["user"]
        url <- map["html_url"]
        state <- map["state"]
    }
    
}
