//
//  CallData.swift
//  ronan-desafio-ios
//
//  Created by Ronan on 11/2/17.
//  Copyright © 2017 RONAN. All rights reserved.
//
// Referencias:
// Udemy - Devslopes
// Projeto - Bruno Eiti


import Foundation
import ObjectMapper

class CallData: Mappable {
    
    var totalCount: Int?
    var itens: [RepositoryMap]? = []
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        totalCount <- map["total_count"]
        itens <- map["items"]
    }
    
}
