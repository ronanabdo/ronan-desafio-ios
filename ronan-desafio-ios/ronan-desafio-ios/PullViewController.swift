//
//  PullViewController.swift
//  GitPop
//
//  Created by Bruno on 1/15/17.
//  Copyright © 2017 bruno. All rights reserved.
//
// Referencias:
// Udemy - Devslopes
// Projeto - Bruno Eiti


import UIKit

class PullViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    var user: String?
    var repository: String?
    var content = PullContentManager()
    
    @IBOutlet weak var tblPulls: UITableView!
    
    
    //MARK: - UITableView Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return content.getContent().count
        
    }
    
     func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PullCellPull", for: indexPath) as! PullTableViewCell
        
        let item = content.getContent()[indexPath.row]
        
        cell.titlePullLabel.text = item.title
        cell.descPullLabel.text = item.body
        cell.userPullLabel.text = item.user?.login
        
        //use a temporary profile image and load/restore the correct image using SDWebImage for caching
        cell.profPullImg?.sd_cancelCurrentImageLoad()
        let placeholderProfile = UIImage(named: "profile")
        if let urlStr = item.user?.avatar{
            if let url = URL(string: urlStr){
                cell.profPullImg?.sd_setImage(with: url, placeholderImage: placeholderProfile!)
            }else{
                cell.profPullImg?.image = placeholderProfile
            }
        }else{
            cell.profPullImg?.image = placeholderProfile
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selected = content.getContent()[indexPath.row]
        
        if let urlStr = selected.url{
            //open in safari the URL with more info about the pull
            if let url = URL(string: urlStr){
                UIApplication.shared.openURL(url)
            }
        }
        
    }
    
    //MARK: - private Methods
    private func loadData(){
        //only load pull data if every parameter is set
        if let user = user, let repository = repository{
            //start the loading indicator on top of device
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            
            //start the download of the new data needed
            content.askContent(login: user, repository: repository, callBack: { (success) in
                //update UI
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                if success{
                    self.tblPulls.reloadData()
                    
                }
            })
        }
        
    }
    
    //MARK: - ViewController Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        tblPulls.delegate = self
        tblPulls.dataSource = self
        
        self.title = repository
    }
    
    override func viewDidAppear(_ animated: Bool) {
        loadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
}
