//
//  ContentManager.swift
//  ronan-desafio-ios
//
//  Created by Ronan on 11/2/17.
//  Copyright © 2017 RONAN. All rights reserved.
//
// Referencias:
// Udemy - Devslopes
// Projeto - Bruno Eiti


import UIKit

class ContentManager: NSObject {
    
    private  var lastPageLoaded = 0;
    private  var loadedRepositories = [RepositoryMap]()
    internal let itensPerPage = 20
    private  var loading = false
    private  let server = GitServer()
    private  var nextLoad = 0
    
    public  func getContent() -> [RepositoryMap]{
        return loadedRepositories;
    }
    
    //everytime the content manager need more data to send to UI,
    //this methods download more data and make it ready
    public  func askMoreContent(callBack:@escaping (Bool)->Void) -> Void{
        
        if(!loading){
            
            loading = true
            lastPageLoaded += 1
            server.getJavaRep(page: lastPageLoaded, perPage: itensPerPage, callback: { (queryResult) in
                if let queryResult = queryResult{
                    self.processDownloadedData(query: queryResult)
                    callBack(true)
                }else{
                    print("cant load more data")
                    callBack(false)
                }
                self.loading = false
            })
            
        }
    }
    public  func getNextLoad() -> Int{
        return nextLoad;
    }
    private  func processDownloadedData(query:CallData){
        
        if let itens = query.itens{
            if itens.count > 0{
                for item in itens{
                    loadedRepositories.append( item)
                }
                self.nextLoad = self.lastPageLoaded * self.itensPerPage - (itensPerPage/2)
            }else{
                lastPageLoaded  -= 1
            }
            
        }else{
            lastPageLoaded  -= 1
        }
        
    }

    
    
}
