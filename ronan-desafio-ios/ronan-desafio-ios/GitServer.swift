//
//  GitServer.swift
//  ronan-desafio-ios
//
//  Created by Ronan on 11/2/17.
//  Copyright © 2017 RONAN. All rights reserved.
//
// Referencias:
// Udemy - Devslopes
// Projeto - Bruno Eiti


import UIKit
import Alamofire
import ObjectMapper


class GitServer: NSObject {
    
    internal let base = "https://api.github.com"
    
    func getJavaRep(page:Int,perPage:Int,callback:@escaping (CallData?) -> Void) -> Void {
      
        Alamofire.request("\(base)/search/repositories" ,
            parameters:["q":"language:Java",
                        "sort":"stars",
                        "page":page,
                        "per_page":perPage]
            ).responseJSON { (response) in
                
                
                if let data = response.data {
                    if let  s = String(data: data, encoding: .utf8){
                        if let f = CallData(JSONString:s ){
                            
                            callback(f)
                        }else{
                            print("Parsing Repositories problem")
                            callback(nil)
                        }
                    }else{
                        print("Content Repositories Problem!")
                        callback(nil)
                    }
                }else{
                    print("Server/Internet Repositories Problem!")
                    callback(nil)
                }
                
        }
        
    }
    
    func getJavaPulls(login:String,repository:String,callback:@escaping ([PullData]?)->Void) -> Void {
        
        Alamofire.request("\(base)/repos/\(login)/\(repository)/pulls").responseJSON { (response) in
            
            if let data = response.data {
                if let s = String(data: data, encoding: .utf8){
                    if let list = Mapper<PullData>().mapArray(JSONString: s){
                        callback(list)
                    }else{
                        print("parsing pull Problem!")
                    }
                }else{
                    print("Content Pull Problem!")
                    callback(nil)
                }
            }else{
                print("Server/Internet Pull Problem!")
                callback(nil)
            }
            
        }
        
    }
    
    
    
    
    
}
