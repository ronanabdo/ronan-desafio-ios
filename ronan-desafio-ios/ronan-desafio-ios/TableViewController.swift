//
//  TableViewTableViewController.swift
//  ronan-desafio-ios
//
//  Created by Ronan on 11/2/17.
//  Copyright © 2017 RONAN. All rights reserved.
//
// Referencias:
// Udemy - Devslopes
// Projeto - Bruno Eiti


import UIKit
import SDWebImage

class TableViewController: UITableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

       
    }
    
    override func viewDidAppear(_ animated: Bool) {
       loadData()
        
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    let content = ContentManager()
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return content.getContent().count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "GitJavaCell", for: indexPath) as! TableViewCell

        if content.getNextLoad()<indexPath.row{
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            content.askMoreContent(callBack: { (sucesso) in
                self.tableView.reloadData()
                
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            })
        }
        
        let item = content.getContent()[indexPath.row]

        cell.titleLabel.text = item.name
        cell.descLabel.text = item.description
        cell.forkLabel.text = "\(item.fork!)"
        cell.starLabel.text = "\(item.stars!)"
        cell.userLabel.text = item.owner?.login
        
        cell.profImage?.sd_cancelCurrentImageLoad()
        let placeholderProfile = UIImage(named: "profile")
        if let urlStr = item.owner?.avatar{
            if let url = URL(string: urlStr){
                cell.profImage?.sd_setImage(with: url, placeholderImage: placeholderProfile!)
            }else{
                cell.profImage?.image = placeholderProfile
            }
        }else{
            cell.profImage?.image = placeholderProfile
        }

        

        return cell
    }
    
     override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selected = content.getContent()[indexPath.row]
        
        if let user = selected.owner?.login, let repository = selected.name{
            //send data of the selected repository to PullViewController where more date will be presented
            let next = storyboard?.instantiateViewController(withIdentifier: "PullViewController") as! PullViewController
            next.user = user
            next.repository = repository
            self.navigationController?.pushViewController(next, animated: true)
        }
    }
    
    private func loadData(){
        //show in UI the loading alert
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let alert = UIAlertController(title: "Loading", message: "Please Wait...", preferredStyle: .alert)
        self.present(alert, animated: true, completion: nil)
        
        //ask data for the first time
        content.askMoreContent { (success) in
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
            //when the data is ready update UI to show the firt lines
            if success{
                
                self.tableView.reloadData()
                alert.dismiss(animated: true, completion: nil)
            }else{
                //if had problems loading data, ask the user to retry
                alert.message = "Failed to load"
                alert.addAction(UIAlertAction(title: "Retry", style: .default, handler: { (a) in
                    self.loadData()
                }))
                
            }
        }
    }
    
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
