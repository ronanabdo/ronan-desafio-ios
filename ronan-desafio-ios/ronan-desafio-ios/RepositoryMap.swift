//
//  RepositoryMap.swift
//  ronan-desafio-ios
//
//  Created by Ronan on 11/2/17.
//  Copyright © 2017 RONAN. All rights reserved.
//
// Referencias:
// Udemy - Devslopes
// Projeto - Bruno Eiti


import Foundation
import ObjectMapper


class RepositoryMap: Mappable {
    
    var id: Int?
    var name: String?
    var fork: Int?
    var stars: Int?
    var description: String?
    var owner : UserData?
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        fork <- map["forks_count"]
        stars <- map["stargazers_count"]
        description <- map["description"]
        owner <- map["owner"]
    }
    
}
