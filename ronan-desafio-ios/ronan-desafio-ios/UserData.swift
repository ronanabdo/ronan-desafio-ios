//
//  UserData.swift
//  ronan-desafio-ios
//
//  Created by Ronan on 11/2/17.
//  Copyright © 2017 RONAN. All rights reserved.
//
// Referencias:
// Udemy - Devslopes
// Projeto - Bruno Eiti

import ObjectMapper


class UserData: Mappable {
    
    var login: String?
    var avatar: String?
    
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        login <- map["login"]
        avatar <- map["avatar_url"]
    }
}
