//
//  PullTableViewCell.swift
//  ronan-desafio-ios
//
//  Created by Ronan on 11/3/17.
//  Copyright © 2017 RONAN. All rights reserved.
//
// Referencias:
// Udemy - Devslopes
// Projeto - Bruno Eiti


import UIKit

class PullTableViewCell: UITableViewCell {

    @IBOutlet weak var titlePullLabel: UILabel!
    @IBOutlet weak var descPullLabel: UILabel!
    @IBOutlet weak var userPullLabel: UILabel!
    @IBOutlet weak var profPullImg: UIImageView!
    
    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
